import React from 'react';
import PanTool from '@material-ui/icons/PanTool';
import AcUnit from '@material-ui/icons/AcUnit';
import Create from '@material-ui/icons/Create';
import NetworkCheck from '@material-ui/icons/NetworkCheck';
import BlurOn from '@material-ui/icons/BlurOn';
import Radio from '@material-ui/icons/Radio';
import { Box, styled } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';

const EvidencesWrapper = styled(Box)({
  float: 'right',
});

const GhostEvidences = ({ evidences }) => {
  return (
    <EvidencesWrapper>
      {evidences.fingerprints && <PanTool style={{ margin: 5 }} />}
      {evidences.freezingTemperature && <AcUnit style={{ margin: 5 }} />}
      {evidences.ghostWriting && <Create style={{ margin: 5 }} />}
      {evidences.emf5 && <NetworkCheck style={{ margin: 5 }} />}
      {evidences.ghostOrbs && <BlurOn style={{ margin: 5 }} />}
      {evidences.spiritBox && <Radio style={{ margin: 5 }} />}
    </EvidencesWrapper>
  );
};

export default GhostEvidences;
