import { Box } from '@material-ui/core';
import React from 'react';
import GhostCard from '../GhostCard/GhostCard';

const checkEvidence = (evidenceName, ghost, filters) =>
  !filters[evidenceName] || (filters[evidenceName] && ghost.evidences[evidenceName]);

const filterGhosts = (ghosts, filters) => {
  return ghosts.filter(
    (ghost) =>
      checkEvidence('emf5', ghost, filters) &&
      checkEvidence('fingerprints', ghost, filters) &&
      checkEvidence('freezingTemperature', ghost, filters) &&
      checkEvidence('ghostOrbs', ghost, filters) &&
      checkEvidence('ghostWriting', ghost, filters) &&
      checkEvidence('spiritBox', ghost, filters)
  );
};

const GhostList = ({ ghosts, evidencesToFilter }) => {
  const filteredGhosts = filterGhosts(ghosts, evidencesToFilter);

  return (
    <Box>
      {filteredGhosts.map((ghost) => (
        <GhostCard key={ghost.type} ghost={ghost}></GhostCard>
      ))}
    </Box>
  );
};

export default GhostList;
