import { Card, CardContent, Typography } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import React from 'react';
import GhostEvidences from '../GhostEvidences/GhostEvidences';

const CardWrapper = styled(Card)({
  margin: '10px',
});

const StyledCardContent = styled(CardContent)({
  paddingBottom: '16px',
});

const GhostCard = ({ ghost }) => {
  return (
    <CardWrapper>
      <StyledCardContent>
        <Typography variant="h5" component="h2">
          {ghost.type}
          <GhostEvidences evidences={ghost.evidences} />
        </Typography>
      </StyledCardContent>
    </CardWrapper>
  );
};

export default GhostCard;
