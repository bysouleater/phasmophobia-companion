import { Box, Button, styled } from '@material-ui/core';
import AcUnit from '@material-ui/icons/AcUnit';
import BlurOn from '@material-ui/icons/BlurOn';
import Create from '@material-ui/icons/Create';
import NetworkCheck from '@material-ui/icons/NetworkCheck';
import PanTool from '@material-ui/icons/PanTool';
import Radio from '@material-ui/icons/Radio';
import React from 'react';

const GhostFilterWrapper = styled(Box)({
  margin: '16px',
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-between',
});

const StyledButton = styled(Button)({
  marginBottom: '10px',
});

const GhostFilter = ({ evidences, updateEvidence }) => {
  return (
    <GhostFilterWrapper>
      <StyledButton
        variant={evidences.fingerprints ? 'contained' : 'outlined'}
        color={evidences.fingerprints ? 'primary' : 'default'}
        startIcon={<PanTool />}
        onClick={() => updateEvidence({ fingerprints: !evidences.fingerprints })}
      >
        Huellas
      </StyledButton>
      <StyledButton
        variant={evidences.freezingTemperature ? 'contained' : 'outlined'}
        color={evidences.freezingTemperature ? 'primary' : 'default'}
        startIcon={<AcUnit />}
        onClick={() => updateEvidence({ freezingTemperature: !evidences.freezingTemperature })}
      >
        Temp.
      </StyledButton>
      <StyledButton
        variant={evidences.emf5 ? 'contained' : 'outlined'}
        color={evidences.emf5 ? 'primary' : 'default'}
        startIcon={<NetworkCheck />}
        onClick={() => updateEvidence({ emf5: !evidences.emf5 })}
      >
        EMF5
      </StyledButton>
      <StyledButton
        variant={evidences.ghostWriting ? 'contained' : 'outlined'}
        color={evidences.ghostWriting ? 'primary' : 'default'}
        startIcon={<Create />}
        onClick={() => updateEvidence({ ghostWriting: !evidences.ghostWriting })}
      >
        Escritura
      </StyledButton>
      <StyledButton
        variant={evidences.ghostOrbs ? 'contained' : 'outlined'}
        color={evidences.ghostOrbs ? 'primary' : 'default'}
        startIcon={<BlurOn />}
        onClick={() => updateEvidence({ ghostOrbs: !evidences.ghostOrbs })}
      >
        Orbes
      </StyledButton>
      <StyledButton
        variant={evidences.spiritBox ? 'contained' : 'outlined'}
        color={evidences.spiritBox ? 'primary' : 'default'}
        startIcon={<Radio />}
        onClick={() => updateEvidence({ spiritBox: !evidences.spiritBox })}
      >
        Spirit
      </StyledButton>
    </GhostFilterWrapper>
  );
};

export default GhostFilter;
