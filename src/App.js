import Refresh from '@material-ui/icons/Refresh';
import {
  AppBar,
  IconButton,
  ThemeProvider,
  Toolbar,
  Typography,
  createMuiTheme,
  CssBaseline,
} from '@material-ui/core';
import GhostList from './components/GhostList/GhostList';
import phasmophobia from './phasmophobia.json';
import GhostFilter from './components/GhostFilter/GhostFilter';
import { useState } from 'react';

const initialState = {
  emf5: false,
  fingerprints: false,
  freezingTemperature: false,
  ghostOrbs: false,
  ghostWriting: false,
  spiritBox: false,
};

function App() {
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
    },
  });

  const [evidences, setEvidences] = useState(initialState);

  const updateEvidence = (newEvidences) =>
    setEvidences({
      ...evidences,
      ...newEvidences,
    });

  const resetEvidence = () => setEvidences(initialState);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Phasmophobia Companion
          </Typography>
          <IconButton edge="end" color="inherit" aria-label="menu" onClick={resetEvidence}>
            <Refresh />
          </IconButton>
        </Toolbar>
      </AppBar>
      <GhostFilter evidences={evidences} updateEvidence={updateEvidence} />
      <GhostList ghosts={phasmophobia.ghosts} evidencesToFilter={evidences} />
    </ThemeProvider>
  );
}

export default App;
